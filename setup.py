#
# run with python setup.py build_ext -i
#
from setuptools import setup
import sdist_upip

from iot_utils import __version__ as PackageVersion

setup(name='micropython-iot-utils',
    version=PackageVersion,
    description='micropython-iot-utils',
    long_description ='...',
    long_description_content_type = "text/html",
    author='Axel Wachtler',
    author_email='uracolix@quantentunnel.de',
    url='http://uracoli.nongnu.org',
    packages = ["iot_utils"],
    #package_data={'monedero': ['blackcoin-logo.png']},
    #install_requires=["reportlab"],
    #scripts = ["mk_wallet.py"],
    #data_files=[('.', ['monedero/img/blackcoin-logo.png'])],
    include_package_data=True,
    cmdclass={'sdist': sdist_upip.sdist}
)
